# -*- coding: utf-8 -*-
{
    'name': "Serial Number Lot",
    'author': "Persiscal Consulting LLC",
    'website': "https://www.persiscalconsulting.com",

    'summary': 'Serial Number Lot',
    'description': """
Serial Number by Lot.
Genera un lote de números de series para su uso posterior
    """,

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Sales',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'stock', 'product'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/serial_number_lot_templates.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
    'auto_install': True,
    'application': False,
    'installable': True,
    'license': 'OEEL-1',
}




