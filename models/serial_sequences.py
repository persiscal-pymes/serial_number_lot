# -*- coding: utf-8 -*-
from odoo import models, fields, api, _

class serial_sequences(models.Model):
    _name = 'serial.sequences'
    _rec_name = 'prefix'
    _description = 'Serial Sequences'

    product_id = fields.Many2one('product.product', 'Product')
    prefix = fields.Char(
        related='product_id.default_code',
        string='Default Code'
    )
    next_number = fields.Integer('Next Number')