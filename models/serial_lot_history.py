# -*- coding: utf-8 -*-

from odoo.exceptions import UserError
from odoo import models, fields, api, _
import time
import datetime
import functools
import logging
_logger = logging.getLogger(__name__)

class serial_lot_history(models.Model):
    _name = 'serial.lot.history'
    name = fields.Char('Last Serial Number', required=True, index=True, default='0')
    product_id = fields.Many2one('product.product', 'Product')
    prefix = fields.Char(
        related='product_id.default_code',
        string='Default Code'
    )

    quantity = fields.Integer('Quantity')
    seq_start = fields.Integer('Sequence Start')
    seq_end = fields.Integer('Sequence End')
    serial_start = fields.Char('Serial Start')
    serial_end = fields.Char('Serial End')
    # create_date
    user_id = fields.Many2one('res.users', 'User')


    @api.model
    def create(self, vals):
        _logger.info('Created: ')

        name = vals['name']
        seriales = self.serial_lot_generator(vals)

        vals.update({
            'seq_start': int(name) + 1,
            'seq_end': int(name) + vals['quantity'],
            'serial_start':  str(seriales[0]),
            'serial_end':  str(seriales[-1]),
        })

        return super(serial_lot_history, self).create(vals)

    @api.onchange('product_id')
    def _onchange_product_id(self):
        _logger.info(self.product_id)
        for history in self:

            if not history.product_id:
                return {}

            product = history.product_id
            contador = self.env['serial.sequences']
            code_prefix = product.default_code
            _logger.info(contador)
            id_cont = contador.search([('prefix', '=', code_prefix)])
            _logger.info(id_cont)

            if id_cont:
                history.name = contador.browse( id_cont.id ).next_number
            else:
                history.name = '0'

            # return {
            #     'value': {
            #         'name': name
            #     }
            # }

    # def onchange_product(self, cr, uid, ids, product_id, context=None):
    #     producto = self.pool.get('product.product').browse(cr,uid,product_id)
    #     contador = self.pool.get('serial.sequences')

    #     code_prefix = producto.default_code
    #     id_cont = contador.search(cr,uid,[('prefix','=', code_prefix)])

    #     if id_cont :
    #         name = (contador.browse(cr,uid,id_cont[0]).next_number)
    #     else:
    #         name = '0'

    #     return {'value' : {'name' : name}}
    # @api.one()
    def serial_lot_generator(self, create_vals):
        # self.ensure_one()
        serial_ret = []
        name = create_vals['name']
        product_id = create_vals['product_id']
        quantity = create_vals['quantity']

        lot_ref = self.env['stock.production.lot']
        product = self.env['product.product'].browse(product_id)
        contador = self.env['serial.sequences']

        code_prefix = product.default_code

        id_cont = contador.search([('prefix','=', code_prefix)])

        #~ nos fijamos si existe un secuenciador iniciado y asociado a ese producto
        #~ si no existe lo creamos usando como prefijo el codigo del producto
        if id_cont:
            sequence = contador.browse( id_cont.id ).next_number

            if sequence != int(name):
                if name.isdigit():
                    id_cont.write({
                        'next_number' : int(name)+1
                    })
                else:
                    raise UserError('Error: Serial Number must be an integer')

            sequence = int(name)
        else:
            id_cont = contador.create({
                'product_id': product_id,
                'prefix': code_prefix,
                'next_number': int(name)
            })

            sequence = contador.browse( id_cont.id ).next_number


        #~ Cambiamos el formato de la fecha para q sea mas facil tomar el mes y el dia
        date = datetime.datetime.strptime(create_vals['create_date'] or fields.Datetime.now(), '%Y-%m-%d %H:%M:%S').strftime('%Y%m%d')
        if not create_vals['quantity']:
            raise UserError('Serial Quantity cannot be zero')

        for i in range(quantity):

            codigo = list(str(sequence+(i+1)))

            #~ armamos el serial segun lo pedido
            #~ Seria bueno usar bien el modulo datetime de las fechas y no a lo negro como ahora
            #~ Al serial le sacamos el digito unidad y volvemos a armar el resto
            unidad = codigo.pop(-1)
            resto = functools.reduce((lambda x, y: x + y),codigo, '')

            serial = date[4:6] + resto + '-' + unidad + date[2:4]
            serial_ret.append(serial)
            lot_ref.create({
                'product_id': product_id,
                'name': serial,
            })

        #~ Actualizamos el contador
        id_cont.write({
            'next_number': sequence + quantity
        })
        return serial_ret
